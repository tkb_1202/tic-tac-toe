package com.fivepointsomeone.tic_tac_toe.gameplay;

public class Dimensions {

    private final static int BOARD_DIMENSION = 3;
    private final static int ZERO_WIN_PARAMETER = 1111111;
    private final static int CROSS_WIN_PARAMETER = 2222222;
    private final static int DRAW_PARAMETER = 9;

    public static int getZeroWinParameter() {
        return ZERO_WIN_PARAMETER;
    }

    public static int getCrossWinParameter() {
        return CROSS_WIN_PARAMETER;
    }

    public static int getDrawParameter() {
        return DRAW_PARAMETER;
    }

    public static int getBoardDimension() {
        return BOARD_DIMENSION;
    }
}
