package com.fivepointsomeone.tic_tac_toe.gameplay;

import android.content.Context;
import android.widget.ImageView;

public class Board {
    ImageView[][] imageView = new ImageView[3][3];
    private Square[][] squares = new Square[3][3];

    public Square[][] getSquares() {
        return squares;
    }

    public void setSquares(Square[][] squares) {
        this.squares = squares;
    }

    public Board(ImageView[][] imageView) {
        for(int i = 0; i < Dimensions.getBoardDimension(); i++) {
            for(int j = 0; j < Dimensions.getBoardDimension(); j++) {
                squares[i][j] = new Square(imageView[i][j]);
            }
        }
    }
}
