package com.fivepointsomeone.tic_tac_toe;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        android.widget.Button button = findViewById(R.id.buttonStartGame);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonStartGameOnClickListener();
            }
        });

    }

        private void buttonStartGameOnClickListener() {
        Intent intent = new Intent(this, DoublePlayerGameBoardActivity.class);
        startActivity(intent);
    }

}
