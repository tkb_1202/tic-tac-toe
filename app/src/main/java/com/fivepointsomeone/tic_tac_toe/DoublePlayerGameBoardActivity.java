package com.fivepointsomeone.tic_tac_toe;

import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.fivepointsomeone.tic_tac_toe.gameplay.Board;
import com.fivepointsomeone.tic_tac_toe.gameplay.Dimensions;

public class DoublePlayerGameBoardActivity extends AppCompatActivity {


    private static String LOG_TAG = "DoublePlayerGameBoardActivity";

    private final static int DOT_WINS = 1;
    private final static int CROSS_WINS = -1;

    Board board;
    int turnCounter = 0;

    ImageView[][] boardSquares = new ImageView[3][3];
    Button resetButton1, resetButton2;
    int[][] boardMatrix = new int[3][3];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_double_player_game_board);

        resetButton1 = findViewById(R.id.buttonResetFront);
        resetButton2 = findViewById(R.id.buttonResetRear);

        initializeBoard();

        resetButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBoard();
            }
        });
        resetButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetBoard();
            }
        });

        board = new Board(boardSquares);

        Log.i(LOG_TAG, "board initialized");
        Toast.makeText(this, "Player One is O", Toast.LENGTH_SHORT).show();


    }

    private void initializeBoard() {
        for (int i = 0; i < Dimensions.getBoardDimension(); i++) {
            for (int j = 0; j < Dimensions.getBoardDimension(); j++) {
                String viewName = "imageButton" + Integer.toString(i * Dimensions.getBoardDimension() + j);
                Log.i(LOG_TAG, viewName);
                int resId = getResources().getIdentifier(viewName, "id", getPackageName());
                boardSquares[i][j] = findViewById(resId);
                final int tempI = i;
                final int tempJ = j;
                boardSquares[i][j].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        handleOnclickEvent(v, tempI, tempJ);
                    }
                });
            }
        }
    }

    private void handleOnclickEvent(View v, int i, int j) {
//        Log.i(LOG_TAG, "handleOnclickEvent() invoked. turnCounter = " + Integer.toString(turnCounter));
        if (turnCounter < 9) {
            int imageResource;
            Log.i(LOG_TAG, "Turn coounter: " + Integer.toString(turnCounter));
            Drawable res;
            int temp = turnCounter % 2;
            if(boardMatrix[i][j] == 0) {
                switch (temp) {
                    case 1:
//                    Log.i(LOG_TAG, "The cross");
                        boardMatrix[i][j] = -1;
                        imageResource = getResources().getIdentifier("@drawable/icon_cross", null, getPackageName());
                        res = getResources().getDrawable(imageResource);
                        ((ImageView) v).setImageDrawable(res);
                        break;
                    case 0:
//                    Log.i(LOG_TAG, "The zero");
                        boardMatrix[i][j] = 1;
                        imageResource = getResources().getIdentifier("@drawable/icon_zero", null, getPackageName());
                        res = getResources().getDrawable(imageResource);
                        ((ImageView) v).setImageDrawable(res);
                        break;
                }
                turnCounter++;
                int hasPlayerWon = 0;
                if (turnCounter >= 5) {
                    hasPlayerWon = checkForWin();
                }
                Log.i(LOG_TAG, "hasPlayerWon: " + Integer.toString(hasPlayerWon));
                if (turnCounter == 9 && (hasPlayerWon!= 1 && hasPlayerWon != -1)) {
                    postDrawAction();
                }
            } else {
                Toast.makeText(getBaseContext(), "Mark blank square", Toast.LENGTH_SHORT).show();
            }

        }

    }

    private int checkForWin() {

        Log.i(LOG_TAG, "checkForWin() invoked");
//        printMatrix();
//        first row in winnerIndices is start row and column, second row is end row and column.
        int[][] winnerIndices = new int[2][2];
        int i,j;
        int winner = 0;
        String toast = "";
        String message = "";
        boolean hasWon = false;
        for(i = 0; i < Dimensions.getBoardDimension(); i++) {
            winnerIndices[0][0] = i;
            winnerIndices[0][1] = 0;
            int sum = 0;
            for(j = 0; j < Dimensions.getBoardDimension(); j++) {
                sum += boardMatrix[i][j];

                if(sum == Dimensions.getBoardDimension()) {
                    toast = "row " + Integer.toString(i) + " won for zero";
                    message = "Player one won";
                    winnerIndices[1][0] = i;
                    winnerIndices[1][1] = Dimensions.getBoardDimension() - 1;
                    hasWon = true;
                    winner = 1;
                    break;
                }
                if(sum ==  -1*(Dimensions.getBoardDimension())) {
                    toast = "row " + Integer.toString(i) + " won for cross";
                    message = "Player two won";
                    winnerIndices[1][0] = i;
                    winnerIndices[1][1] = Dimensions.getBoardDimension() - 1;
                    hasWon = true;
                    winner = -1;
                    break;
                }

            }


            if(hasWon) {
//                Toast.makeText(getBaseContext(), toast, Toast.LENGTH_SHORT).show();
                showAlertDialog("Game finished!", message, "Reset", "Cancel");
                highlightWinner(winnerIndices, winner);
                return winner;
            }

//            Log.i(LOG_TAG, Integer.toString(sum));
        }

        for(i = 0; i < Dimensions.getBoardDimension(); i++) {
            int sum = 0;
            winnerIndices[0][0] = 0;
            winnerIndices[0][1] = i;
            for(j = 0; j < Dimensions.getBoardDimension(); j++) {
                sum += boardMatrix[j][i];
            }
            if(sum == Dimensions.getBoardDimension()) {
//                Toast.makeText(getBaseContext(), "column" + Integer.toString(i) + " won for zero", Toast.LENGTH_SHORT).show();
                winnerIndices[1][0] = Dimensions.getBoardDimension() - 1;
                winnerIndices[1][1] = i;
                winner = 1;
                highlightWinner(winnerIndices, winner);
                showAlertDialog("Game finished!", "Player one won", "Reset", "Cancel");
                return winner;
            }
            if(sum ==  -1*(Dimensions.getBoardDimension())) {
//                Toast.makeText(getBaseContext(), "column" + Integer.toString(i) + " won for cross", Toast.LENGTH_SHORT).show();
                winnerIndices[1][0] = Dimensions.getBoardDimension() - 1;
                winnerIndices[1][1] = i;
                winner = -1;
                highlightWinner(winnerIndices, winner);
                showAlertDialog("Game finished!", "Player two won", "Reset", "Cancel");
                return winner;
            }
        }

        int diagonal1Sum = 0;
        int diagonal2Sum = 0;
        for(i = 0; i < Dimensions.getBoardDimension(); i++) {
            diagonal1Sum += boardMatrix[i][i];
            diagonal2Sum += boardMatrix[i][Dimensions.getBoardDimension() - (i+1)];

            if(diagonal1Sum == Dimensions.getBoardDimension()) {
//                Toast.makeText(getBaseContext(), "Diagonal 1 won with zero", Toast.LENGTH_SHORT).show();
                showAlertDialog("Game finished!", "Player one won", "Reset", "Cancel");
                winnerIndices[0][0] = 0;
                winnerIndices[0][1] = 0;
                winnerIndices[1][0] = Dimensions.getBoardDimension() - 1;
                winnerIndices[1][1] = Dimensions.getBoardDimension() - 1;
                winner = 1;
                highlightWinner(winnerIndices, winner);
                return winner;

            } else if(diagonal1Sum == -1*Dimensions.getBoardDimension()) {
//                Toast.makeText(getBaseContext(), "Diagonal 1 won with cross", Toast.LENGTH_SHORT).show();
                winnerIndices[0][0] = 0;
                winnerIndices[0][1] = 0;
                winnerIndices[1][0] = Dimensions.getBoardDimension() - 1;
                winnerIndices[1][1] = Dimensions.getBoardDimension() - 1;
                winner = -1;
                highlightWinner(winnerIndices, winner);
                showAlertDialog("Game finished!", "Player two won", "Reset", "Cancel");
                return winner;
            }

            if(diagonal2Sum == Dimensions.getBoardDimension()) {
//                Toast.makeText(getBaseContext(), "Diagonal 2 won with zero", Toast.LENGTH_SHORT).show();
                winnerIndices[0][0] = 0;
                winnerIndices[0][1] = Dimensions.getBoardDimension() - 1;
                winnerIndices[1][0] = Dimensions.getBoardDimension() - 1;
                winnerIndices[1][1] = 0;
                winner = 1;
                highlightWinner(winnerIndices, winner);
                showAlertDialog("Game finished!", "Player one won", "Reset", "Cancel");
                return winner;
            } else if(diagonal2Sum == -1*Dimensions.getBoardDimension()) {
//                Toast.makeText(getBaseContext(), "Diagonal 2 won with cross", Toast.LENGTH_SHORT).show();
                winnerIndices[0][0] = 0;
                winnerIndices[0][1] = Dimensions.getBoardDimension() - 1;
                winnerIndices[1][0] = Dimensions.getBoardDimension() - 1;
                winnerIndices[1][1] = 0;
                winner = -1;
                highlightWinner(winnerIndices, winner);
                showAlertDialog("Game finished!", "Player two won", "Reset", "Cancel");
                return  winner;
            }

        }

        return 0;
    }

    private void printMatrix() {
        String matrix = "";
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                matrix = matrix + Integer.toString(boardMatrix[i][j]) + " ";
            }
            matrix = matrix + "\n";
        }

        Log.i(LOG_TAG, "Matrix:\n" + matrix);
    }

    private void postWinAction(int i) {
        String alertMessage;
        if (i == DOT_WINS) {
            alertMessage = "Zero wins the Game";
        } else {
            alertMessage = "Creoss wins the Game";
        }

//        Toast.makeText(getBaseContext(), alertMessage, Toast.LENGTH_SHORT).show();
        showAlertDialog("Game finished!", alertMessage, "RESET BOARD", "CANCEL");
//        resetBoard();
//        TODO 1: Highlight the range line has won, stop taking inputs, give option to reset
    }

    private void postDrawAction() {
        Log.i(LOG_TAG, "postDrawAction() called");

        showAlertDialog("Game finished", "The game is Draw", "Reset", "Cancel");
//        resetBoard();
//        TODO 2: Reset the game. Show that it is draw
    }

    private void resetBoard() {
        boardMatrix = new int[3][3];
        turnCounter = 0;
        for (int i = 0; i < Dimensions.getBoardDimension(); i++) {
            for (int j = 0; j < Dimensions.getBoardDimension(); j++) {
                boardSquares[i][j].setImageResource(R.color.colorPrimary);
//                boardSquares[i][j].setOnClickListener(null);
                initializeBoard();
            }
        }

    }

    private void showAlertDialog(String title, String message, String yesMessage, String noMessage) {
        AlertDialog.Builder builder;
        builder = new AlertDialog.Builder(this);

        builder.setMessage(message).setCancelable(false)
                .setPositiveButton(yesMessage, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.i(LOG_TAG, "Yes tapped in alertBox");
                        resetBoard();
                    }
                }).setNegativeButton(noMessage, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Log.i(LOG_TAG, "No tapped in alertBox");
                for(int i = 0; i < Dimensions.getBoardDimension(); i++) {
                    for(int j = 0; j < Dimensions.getBoardDimension(); j++) {
                        boardSquares[i][j].setOnClickListener(null);
                    }
                }
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.setTitle(title);
        alertDialog.show();
    }
//    first row is: starting row,column
//    second row is: ending row, column
    private void highlightWinner(int[][] indices, int winner) {
        Log.i(LOG_TAG, "indices:\n" + Integer.toString(indices[0][0]) + " " + Integer.toString(indices[0][1]) + " " + Integer.toString(indices[1][0]) + " " + Integer.toString(indices[1][1]));
        if(indices[0][0] == indices[1][0]) {
//            row wins
            Log.i(LOG_TAG, "In highlightWinner: Row " + Integer.toString(indices[0][0]) + " wins");
            for(int i = 0; i < Dimensions.getBoardDimension(); i++) {
                if(winner == 1) {
                    boardSquares[indices[0][0]][i].setImageResource(R.drawable.icon_zero_red);
                } else {
                    boardSquares[indices[0][0]][i].setImageResource(R.drawable.icon_cross_red);
                }

            }
        } else if(indices[0][1] == indices[1][1]) {
//            column wins
            Log.i(LOG_TAG, "In highlightWinner: Column " + Integer.toString(indices[1][1]) + " wins");
            for(int i = 0; i < Dimensions.getBoardDimension(); i++) {
                if(winner == 1) {
                    boardSquares[i][indices[1][1]].setImageResource(R.drawable.icon_zero_red);
                } else {
                    boardSquares[i][indices[1][1]].setImageResource(R.drawable.icon_cross_red);
                }
            }
        } else if(indices[0][0] == indices[0][1] && indices[1][1] == indices[1][0]) {
//            diagonal 1 wins
            Log.i(LOG_TAG, "In highlightWinner: Diagonal 1 wins");
            for(int i = 0; i < Dimensions.getBoardDimension(); i++) {
                if(winner == 1) {
                    boardSquares[i][i].setImageResource(R.drawable.icon_zero_red);
                } else {
                    boardSquares[i][i].setImageResource(R.drawable.icon_cross_red);
                }

            }
        } else if(indices[0][0] == indices[1][1] && indices[0][1] == indices[1][0]) {
//            diagonal 2 wins
            Log.i(LOG_TAG, "In highlightWinner: Diagonal 2 wins");
            for(int i = 0; i < Dimensions.getBoardDimension(); i++) {
                if(winner == 1) {
                    boardSquares[Dimensions.getBoardDimension() - i -1][i].setImageResource(R.drawable.icon_zero_red);
                } else {
                    boardSquares[Dimensions.getBoardDimension() - i -1][i].setImageResource(R.drawable.icon_cross_red);
                }

            }
        }
    }
}
